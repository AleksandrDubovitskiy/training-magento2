<?php

namespace Training\TestOM\Model;

/**
 * Class Test
 */
class Test
{
    private $manager;

    private $arrayList;

    private $name;

    private $number;

    private $managerFactory;

    /**
     * Test constructor.
     *
     * @param ManagerInterface $manager
     * @param array $arrayList
     * @param $name
     * @param int $number
     */
    public function __construct(
        \Training\TestOM\Model\ManagerInterface $manager,
        \Training\TestOM\Model\ManagerInterfaceFactory $managerFactory,
        $name,
        array $arrayList,
        int $number
    )
    {
        $this->manager = $manager;
        $this->managerFactory = $managerFactory;
        $this->arrayList = $arrayList;
        $this->name = $name;
        $this->number = $number;
    }

    public function log()
    {
        print_r(get_class($this->manager));
        echo '<br>';
        print_r($this->arrayList);
        echo '<br>';
        print_r($this->name);
        echo '<br>';
        print_r($this->number);

        echo '<br>';
        $newManager = $this->manager->create();
        print_r(get_class($newManager));
    }
}