<?php

namespace Training\TestOM\Model;

/**
 * Interface ManagerInterface
 *
 * @package Training\TestOM\Model
 */
interface ManagerInterface
{
   public function create();

   public function get();
}