<?php

namespace Training\TestOM\Model;

/**
 * Class PlayWithTest
 *
 * @package Training\TestOM\Model
 */
class PlayWithTest
{
    private $testObject;

    private $testObjectFactory;

    private $manager;

    /**
     * PlayWithTest constructor.
     */
    public function __construct(
        \Training\TestOM\Model\Test $test,
        \Training\TestOM\Model\TestFactory $testFactory,
        \Training\TestOM\Model\ManagerCustomImplementation $manager
    )
    {
        $this->testObject = $test;
        $this->testObjectFactory = $testFactory;
        $this->manager = $manager;
    }

    public function run()
    {
        $this->testObject->log();

        $customArrayList = [
            'item_10' => 'Item_10_text',
            'item_20' => 'Item_20_text'
        ];

        $newDataForTestObject = [
            'arrayList' => $customArrayList,
            'manager' => $this->manager
        ];

        $newTestObject = $this->testObjectFactory->create($newDataForTestObject);
        $newTestObject->log();
    }
}