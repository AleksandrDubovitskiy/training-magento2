<?php

namespace Training\TestOM\Model;

/**
 * Class Manager
 *
 * @package Training\TestOM\Model
 */
class Manager implements ManagerInterface
{

    public function create()
    {}

    public function get()
    {}
}