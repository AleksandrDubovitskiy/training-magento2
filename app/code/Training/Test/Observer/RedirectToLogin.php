<?php

namespace Training\Test\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
/**
 * Class RedirectToLogin
 *
 * @package Training\Test\Observer
 */
class RedirectToLogin implements ObserverInterface
{
    const FULL_ACTION_NAME = 'catalog_product_view';

    public $redirect;

    public $actionFlag;

    public function __construct(
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\App\ActionFlag $actionFlag
    )
    {
        $this->redirect = $redirect;
        $this->actionFlag = $actionFlag;
    }

    /**
     * Redirect to login page.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        if ($request->getFullActionName() === self::FULL_ACTION_NAME) {
            $controller = $observer->getEvent()->getControllerAction();
            $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $this->redirect->redirect($controller->getResponse(), 'customer/account/login');
        }
    }
}