<?php

namespace Training\Test\Plugin\Block;

/**
 * Class Block
 *
 * @package Training\Test\Plugin\Block
 */
class Template
{
    const TOP_SEARCH_LAYOUT = 'top.search';

    /**
     * Paragraph 1.8
     *
     * @param \Magento\Framework\View\Element\Template $subject
     * @param $result
     * @return string
     */
    public function _afterToHtml(
        \Magento\Framework\View\Element\Template $subject,
        $result
    )
    {
       $result = sprintf(
    "<div>
                <p>%s</p>
                <p>%s</p>
                %s
            </div>",
            $subject->getTemplate(),
            get_class($subject),
            $result
        );

       return $result;
    }

    /**
     * Paragraph 1.9
     *
     * @param \Magento\Framework\View\Element\Template $subject
     * @param $result
     *
     * @return string
     */
    public function afterToHtml(
        \Magento\Framework\View\Element\Template $subject,
        $result
    )
    {
        if ($subject->getNameInLayout() === self::TOP_SEARCH_LAYOUT) {
            $result = sprintf(
                "<div>
                <p>%s</p>
                <p>%s</p>
                %s
            </div>",
                $subject->getTemplate(),
                get_class($subject),
                $result
            );
        }

        return $result;
    }
}