<?php

namespace Training\Test\Plugin;

/**
 * Class Url
 *
 * @package Training\Test\Plugin
 */
class Url
{
    const CUSTOMER_ACCOUNT_CREATE = 'customer/account/create';

    const CUSTOMER_ACCOUNT_LOGIN = 'customer/account/login';

    public function beforeGetUrl(
        \Magento\Framework\UrlInterface $subject,
        $routePath = null,
        $routeParams = null
    )
    {
        if ($routePath === self::CUSTOMER_ACCOUNT_CREATE) {
            return [self::CUSTOMER_ACCOUNT_LOGIN, $routeParams];
        }
    }
}