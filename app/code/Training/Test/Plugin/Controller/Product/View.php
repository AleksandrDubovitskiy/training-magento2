<?php

namespace Training\Test\Plugin\Controller\Product;

/**
 * Class View
 *
 * @package Training\Test\Plugin\Controller\Product
 */
class View
{
    protected $customerSession;

    protected $redirectFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\RedirectFactory $resultFactory
    )
    {
        $this->customerSession = $customerSession;
        $this->redirectFactory = $resultFactory;
    }

    public function aroundExecute(
        \Magento\Catalog\Controller\Product\View $subject,
        \Closure $proceed
    )
    {
        if (!$this->customerSession->isLoggedIn()) {
            return $this->redirectFactory->create()->setPath('customer/account/login');
        }

        return $proceed();
    }
}